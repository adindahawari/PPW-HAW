from django.db import models

# Create your models here.
class Schedule(models.Model):
    date = models.DateTimeField()
    activities = models.TextField()
    place = models.TextField()
    category = models.TextField(choices = ((" urgent", "urgent"), (" super urgent", "super urgent"), (" extra urgent", "extra urgent")))
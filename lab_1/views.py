from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from datetime import datetime, date
from .forms import ScheduleForm
from .models import Schedule
# Enter your name here
mhs_name = 'Adinda Raisha Hanief Hawari' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 9, 14) #TODO Implement this, format (Year, Month, Date)
npm = 1706021732 # TODO Implement this
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm}
    return render(request, 'index_lab1.html', response)

def home(request):
    response = {}
    return render(request, 'Homepage.html', response)

def about(request):
    response = {}
    return render(request, 'About.html', response)

def family(request):
    response = {}
    return render(request, 'Family.html', response)

def form(request):
    response = {}
    return render(request, 'Form.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

def form_sched(request):
    response = {'form': ScheduleForm, 'sched': Schedule.objects.all()}
    return render(request, 'formz.html', response)

def create_sched(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ScheduleForm(request.POST)
        # check whether it's valid:
        obj = Schedule()
        obj.date = datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')
        obj.activities = request.POST['activities']
        obj.place = request.POST['place']
        obj.category = request.POST['category']
        obj.save()
        print(form.errors)
        # redirect to a new URL:
        return HttpResponseRedirect(reverse('lab_1:formz'))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ScheduleForm()
    return HttpResponseRedirect(reverse('lab_1:formz'))

def delete_all(request):
    Schedule.objects.all().delete()
    return HttpResponseRedirect(reverse('lab_1:formz'))

from django import forms

class ScheduleForm(forms.Form):
    date = forms.DateTimeField(widget = forms.widgets.DateInput(attrs = {'type': 'datetime-local'}))
    activities = forms.CharField()
    place = forms.CharField()
    category = forms.ChoiceField(choices = (("urgent", "urgent"), ("super urgent", "super urgent"), ("extra urgent", "extra urgent")), label="Category", initial='', widget=forms.Select(), required=True)
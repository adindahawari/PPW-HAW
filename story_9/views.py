from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.http import HttpResponseRedirect
import requests


# Create your views here.
def index(request):
	if request.user.is_authenticated:
		request.session['user'] = request.user.username
		request.session['email'] = request.user.email
		request.session.setdefault('counter',0)
	return render(request, 'bookz.html')

def get_books(request):
	search = request.GET.get('cari')
	url = "https://www.googleapis.com/books/v1/volumes?q=" + str(search)
	data = requests.get(url).json()
	return JsonResponse(data)

def logout(request):
	request.session.flush()
	return HttpResponseRedirect('./books')

def increasefav(request):
	request.session['counter'] = request.session['counter'] + 1
	return HttpResponse(request.session['counter'], content_type='application/json')

def decreasefav(request):
	request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type='application/json')


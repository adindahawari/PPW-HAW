from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

# Create your tests here.
class Story6UnitTest(TestCase):
	def test_root_url_is_exist(self):
		response = Client().get('/lab-9/books')
		self.assertEqual(response.status_code,200)

	def test_using_bookz_template(self):
		response = Client().get('/lab-9/books')
		self.assertTemplateUsed(response,'bookz.html')

	def test_json_url_is_exist(self):
		response = Client().get('/lab-9/data/')
		self.assertEqual(response.status_code,200)

	def test_lab11_using_func(self):
		found = resolve('/lab-9/books')
		self.assertEqual(found.func, index)

	def test_inc_url_is_exist(self):
		found = resolve('/lab-9/increasefav/')
		self.assertEqual(found.func, increasefav)

	def test_dec_url_is_exist(self):
		found = resolve('/lab-9/decreasefav/')
		self.assertEqual(found.func, decreasefav)

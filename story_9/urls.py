from django.urls import re_path
from .views import *
from django.contrib.auth import views
from django.conf.urls import include
#url for app

app_name = "story_9"
urlpatterns = [
    re_path(r'^books$', index, name='index'),
    re_path(r'^data/$', get_books, name='data'),
    re_path(r'^auth/', include('social_django.urls', namespace='social')),
    re_path(r'^logout$', logout , name='logout'),
    re_path(r'^increasefav/', increasefav, name='increasefav'),
	re_path(r'^decreasefav/', decreasefav, name='decreasefav'),
]

from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
# from .models import *
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# import selenium.webdriver.chrome.service as service
# import unittest

#Unitest
class Story6UnitTest(TestCase):
    def test_root_url_is_exist(self):
        response = Client().get('/lab-8/')
        self.assertEqual(response.status_code,200)

    def test_using_aboutMe_template(self):
        response = Client().get('/lab-8/')
        self.assertTemplateUsed(response,'aboutMe.html')

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Ubah Tema', html_response)

#Functional Test
# class Story6FunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25) 
#         super(Story6FunctionalTest,self).setUp()

#     def test_title_position(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url)
#         title = selenium.find_element_by_tag_name('h1')
#         title_loc = title.location
#         print(title_loc)
#         # self.assertEqual(title_loc, {'x': 8, 'y': 21})

#     def tearDown(self):
#         self.selenium.quit()
#         super(Story6FunctionalTest, self).tearDown()
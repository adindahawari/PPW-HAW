from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .forms import SubscribeForm
from .models import Subscribe
from django.utils.encoding import force_text


# Create your tests here.
class Story10UnitTest(TestCase):

	def test_hello_name_is_exist(self):
		response = Client().get('/lab-1/')
		self.assertEqual(response.status_code,200)

	# def test_using_index_func(self):
	# 	found = resolve('/lab-1/')
	# 	self.assertEqual(found.func, index)

	def test_lab1_profile_has_name(self):
		response = Client().get('/')
		string = response.content.decode('utf8')
		self.assertIn('Adinda Raisha Hanief Hawari', string)

	# def test_lab1_profile_has_dateofbirth(self):
	# 	response = Client().get('/')
	# 	string = response.content.decode('utf8')
	# 	self.assertIn('Jakarta, September 14 1999', string)

	def test_lab1_profile_has_Hobby(self):
		response = Client().get('/')
		string = response.content.decode('utf8')
		self.assertIn('Folk Dance and Singing', string)

	def test_lab10_url_is_exist(self):
		response = Client().get('/lab-10/subscribe')
		self.assertEqual(response.status_code, 200)

	def test_lab10_using_subscribe_func(self):
		found = resolve('/lab-10/subscribe')
		self.assertEqual(found.func, subscribe)


	# def test_double_subscriber(self):
	# 	nama = 'abcde'
	# 	password = 'opqrstu'
	# 	email = 'vwxyz@gmail.com'
	# 	go = Client().post('/validate', {'email': email})
	# 	self.assertJSONEqual(
	# 	force_text(go.content),
	# 		{'not_valid': False} #responds false
	# 	)

	# 	# button not disabled, send data to views
	# 	Client().post('/success', {'name': nama, 'password': password, 'email': email})
	# 	subsum = Subscribe.objects.all().count()
	# 	self.assertEqual(subsum, 1)

	# 	# email already taken
	# 	go = Client().post('/validate', {'email': email})
	# 	self.assertJSONEqual(
	# 	str(go.content, encoding='utf8'),
	# 		{'not_valid': True} 
	# 	)

	# 	# not making any new data
	# 	subsum = Subscribe.objects.all().count()
	# 	self.assertEqual(subsum, 1)

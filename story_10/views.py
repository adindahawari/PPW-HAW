# from django.shortcuts import render
# from django.http import JsonResponse
# from .models import *
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .forms import SubscribeForm
from .models import Subscribe


# Create your views here.
# def index(request):
# 	return render(request, 'regis.html')

# def validate(request):
# 	email = request.POST.get('mail')
# 	exists = User.objects.filter(email = email).exists()
# 	return JsonResponse({'exists': exists})

def subscribe(request):
    response = {}
    response['form'] = SubscribeForm()
    return render(request, 'regis.html', response)

@csrf_exempt
def validate(request):
    email = request.POST.get("email")
    data = {'not_valid': Subscribe.objects.filter(email__iexact=email).exists()
    }
    return JsonResponse(data)

def success(request):
    form = SubscribeForm(request.POST or None)
    if (form.is_valid()):
        data = form.cleaned_data
        newsubscriber = Subscribe(name=data['name'], password=data['password'], email=data['email'])
        newsubscriber.save()
        obdata = {'name': data['name']}
    return JsonResponse(obdata)


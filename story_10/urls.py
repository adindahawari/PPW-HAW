from django.urls import re_path
from .views import *
#url for app

app_name = "story_10"
urlpatterns = [
    # re_path(r'^$', index, name='index'),
    re_path('subscribe', subscribe, name='subscribe'),
    re_path('success', success, name='success'),
	re_path('validate', validate),

]
$(document).ready(function() {
    var name = "";
    var password = "";
    $(".btn").hide();
    $("#id_name").change(function() {
        name = $(this).val();
    });
    $("#id_password").change(function() {
        password = $(this).val();
    })
    $("#id_email").change(function() {
        var email = $(this).val();
        console.log(email)
        $.ajax({
            url: "/lab-10/validate",
            data: {
                'email': email,
            },
            method: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data)
                if(data.not_valid) {
                    alert("This email has subscribed this channel");
                } else {
                    if(name.length > 0 && password.length > 5) {
                        $("#submit").removeAttr("disabled");
                        $(".btn").show();
                    }
                }
                
            }
        });
    });
    $("#submit").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
            url:"/lab-10/success",
            data: $("form").serialize(),
            method: 'POST',
            dataType: 'json',
            success: function(data){
                swal({
                    title: "Hello " + data.name,
                    text: 'Thanks for subscribing!',
                    type: 'success',
                    showConfirmButton: false,
                    timer: 2500
                    }) 
            }, error: function() {
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                  }).then(function() {
                      window.location = "/lab-10/subscribe"
                  });
            }
        })
    });
});
